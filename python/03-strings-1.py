hostname1 = "csr01"

# print(dir(hostname))

# capitalize
hostname_cap = hostname1.capitalize()
print(hostname_cap)

# strings are immutable - once you create a string, it cannot be changed

# casefold
hostname2 = "CSR01"

print(hostname1.casefold() == hostname2.casefold())

centered_hostname = hostname1.center(50, '*')
print(centered_hostname)

ip_address = "10.0.0.1"
print(ip_address.count('.'))

print(ip_address.startswith('10'))
print(ip_address.startswith('192'))

print(ip_address.find('0.1'))