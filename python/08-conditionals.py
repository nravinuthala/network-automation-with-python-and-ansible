a = 0
b = 1

if b:
    print('valid')
else:
    print('invalid')

if a != b: # >, <, >=, <=, ==, !=
    print('something')
else:
    print("something else")

vendor = 'cisco'
if 'cis' in vendor:
    print('something')
else:
    print("something else")

age = int(input('Enter age: '))
if age < 18:
    if age > 12:
        print('teen')
    else:
        print('preteen')
elif age >= 18 and age < 60:
    print('adult')
elif age >= 60 and age < 80:
    print('senor citizen')
else:
    print('super senior citizen')

if age > 30:
    pass
