# device_details = {
#     'hostname': 'csr01',
#     'type': 'ios',
#     'vendor': 'cisco',
#     'num_intfcs': 4,
#     'mgmt_ip': '192.168.110.136'
# }

# for a, b in device_details.items():
#      print(a,b)

# for i in range(0, 11):
#      print(f'creating vlan vlan{i}')

# loop control statements
# break
# continue
     
for i in range(0, 11):
    if i % 2 != 0:
        continue
    print(f'creating vlan vlan{i}')
else:
    print("Done creating vlans")


for i in range(0, 100):
    if i > 7:
        break
    print(f'creating vlan vlan{i}')
else:
    print("Done creating vlans")