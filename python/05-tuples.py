tuple1 = ('csr01', 'csr02', 1, 2.0, [1, 2], (1, 2), {1, 2}, {1: 2})

print(tuple1)
print(type(tuple1))

print(tuple1[0:6])

# tuple1[0] = 'csr03'

print(dir(tuple))

print(tuple1.count(1))

print(tuple1.index((1,2)))