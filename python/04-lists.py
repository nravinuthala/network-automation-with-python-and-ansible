# list1 = ['csr01', 'csr02', 1, 2.0, [1, 2], (1, 2), {1, 2}, {1: 2}]

# print(list1)
# print(type(list1))

# print(len(list1))

# # forward indexing
# print(list1[0]) # first
# print(list1[7]) # last

# # backward indexing
# print(list1[-1]) # last
# print(list1[-8]) # first

list2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# print(list2[2:6])
# print(list2[4:])
# print(list2[:7])

# print(dir(list))
# 'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort'

new_num = 10
list2.append(new_num)
print(list2)

# list2.clear()

# print(list2)

# list2_copy1 = list2 # shallow copy

# print(list2_copy1)
# print(list2)

# print(id(list2_copy1))
# print(id(list2))

# list2_copy2 = list2.copy() # deep copy

# print(id(list2_copy2))
# print(id(list2))

# list2.append('11')

# print(list2)
# print(list2_copy1)
# print(list2_copy2)

list3 = [11, 22, 33]

# list2.append(list3)
# list2.extend(list3)

# print(list2)

list2 += list3

print(list2)
# print(list4)

list2.insert(0, 4444)
print(list2)

popped_elem = list2.pop()

print(list2)
print(popped_elem)

list2.remove(4444)

print(list2)

list2.reverse()

print(list2)

list2.sort()
print(list2)



