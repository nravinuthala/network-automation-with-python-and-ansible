# identifiers
# name = "Nagaraj"
# print(name)

# Primitive Data types
# textual data - string - str
# numerical data - 
#   - decimal values - float
#   - interger values - int
# true false -- boolean - bool

name = "Nagaraj" # str
age = 45         # int
height = 5.7     # float
is_awake = True  # bool

print(type(name))
print(type(age))
print(type(height))
print(type(is_awake))

age_s = str(age)
print(age_s)
print(type(age_s))

height_i = int(height)
print(height_i)





