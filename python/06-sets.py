set1 = {'csr01', 'csr02', 1, 2.0, 2.0, 1}

print(set1)
print(type(set1))

# print(dir(set))

# 'add', 'clear', 'copy', 'difference', 'difference_update', 'discard', 'intersection', 'intersection_update', 'isdisjoint', 
# 'issubset', 'issuperset', 'pop', 'remove', 'symmetric_difference', 'symmetric_difference_update', 'union', 'update'

set2 = {1, 2, 3, 4}
set3 = {4, 5, 6}

set4 = set2.union(set3)
set5 = set2.intersection(set3)
print(set4)
print(set5)

set6 = set2.difference(set3)
print(set6)

set2.difference_update(set3)
print(set2)

set2.update(set3)
print(set2)