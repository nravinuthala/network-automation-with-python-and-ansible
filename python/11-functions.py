# def fun_name(args):
#     print(args)

# fun_name("arguments")

# functions with mandatory positional arguments
def connect_to_device(ip, user, pwd):
     print(f"Connection to the device with IP {ip} with {user} and {pwd}")

# connect_to_device("10.0.0.1", 'admin', 'cisco')

# functions with default arguments
def connect_to_device(ip, user='admin', pwd='cisco'):
     print(f"Connection to the device with IP {ip} with {user} and {pwd}")

# connect_to_device("10.0.0.1")
# connect_to_device("10.0.0.1", 'root')
# connect_to_device("10.0.0.1", 'root', 'root')
     
# functions with keyword arguments
def connect_to_device(ip, user, pwd):
     print(f"Connection to the device with IP {ip} with {user} and {pwd}")

args = ['cisco', "10.0.0.1", 'admin']
# connect_to_device(user=args[0], ip=args[1], pwd=args[2])

# functions with variable number of arguments
# print(1)
# print(1, 2)
# print(1, 2, 3)
def connect_to_device(*args): # variable number of args
    #  print(args)
    for arg in args:
        print(arg)

# connect_to_device('10.0.0.1', 22, 'admin', 'cisco')
# connect_to_device('10.0.0.1', 'admin', 'cisco')

def connect_to_device(**kwargs): # variable number of keyword args
    """ This function  accepts variable number of keyword args """
    print(kwargs)

dev_details = {'ip': '10.0.0.1', 'port': 22, 'user': 'admin', 'pwd':'cisco'}

# connect_to_device(**dev_details)



print(__name__)

if __name__ == "__main__":
     connect_to_device(ip='10.0.0.1', port=22)
     print(connect_to_device.__doc__)