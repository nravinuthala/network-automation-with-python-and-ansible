numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

num_sqrs = [x ** 2 for x in numbers if x % 2 == 0]

# for num in numbers:
#     num_sqrs.append(num ** 2)

print(num_sqrs)
