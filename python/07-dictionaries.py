# # device_details = ['csr01', 'ios', 'cisco', 4, '192.168.110.136']

device_details = {
    'hostname': 'csr01',
    'type': 'ios',
    'vendor': 'cisco',
    'num_intfcs': 4,
    'mgmt_ip': '192.168.110.136'
}

# print(device_details)
# print(type(device_details))

# # # print(device_details['hostname1'])

# # print(device_details.get('hostname1'))
# # print(device_details.get('type'))

# # # print(dir(dict))

# # person = {}
# # person['name'] = 'Nagaraj'
# # person['name'] = "Ravinuthala"

# # person2 = dict()
# # print(person2)

# # print(dir(dict))

# # dictionary built-in methods
# # clear', 'copy', 'fromkeys', 'get', 'items', 'keys', 'pop', 'popitem', 'setdefault', 'update', 'values'

# device_items = device_details.items()
# print(type(device_items))
# device_items_list = list(device_items)
# print(device_items_list[0])

# for k,v in device_details.items():
#     print(k,v)

# for k in device_details.keys():
#     print(k,device_details.get(k))

# extra_details = {
#     'os_ver': 15.7
# }

# device_details.update(extra_details)

# print(device_details)


keys = ['name', 'age']

# device_details['csr01']

new_dict = dict().fromkeys(keys, 'default_value')
print(new_dict)

for _,v in device_details.items():
    print(v)

line1 = 'this is a sample sentence'

f1, f2, _, f3  = line1.split()


